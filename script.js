const  map = [  
    "  WWWWW ",  
    "WWW   W ",  
    "WOSB  W ",  
    "WWW BOW ",  
    "WOWWB W ",  
    "W W O WW",  
    "WB XBBOW",  
    "W   O  W",  
    "WWWWWWWW"  
  ];
  const mazeModel= []
  const main = document.getElementById("maze");
  
  // let walls="W";
  // let corridor= " ";
  // let playerLocation= "image";
  // let start= "S";
  // let finish="F";
  
  // let x;
  // let y;

for (let indexRow= 0; indexRow < map.length; indexRow++) {
    let row = map[indexRow];
    let mazeBoard = document.createElement("div");
    mazeBoard.classList.add("mazeRow");
    for (let column = 0; column < row.length; column++) {
      let cell = document.createElement("div");
      cell.dataset.rowIndex = indexRow;
      cell.dataset.cellIndex = column;
      mazeBoard.appendChild(cell);
  
    
        switch (row[column]) {
          case "W":
            cell.classList.add("borderWall");
            cell.dataset.cellType = "border";
            break;
    
          case "S":
            cell.setAttribute("id", "start");
            cell.dataset.cellType = "floor";
            break;
    
          case " ":
            cell.classList.add("walkWay");
            cell.dataset.cellType = "floor";
            break;
    
          case "O":
            cell.classList.add("boxOnStart");
            cell.dataset.cellType = "floor";
            break;
    
          case "B":
            cell.classList.add("walkWay");
            cell.dataset.cellType = "floor";
            const box = document.createElement("div");
            box.classList.add("box");
            box.dataset.cellType = "box";
            cell.appendChild(box);
            break;

          case "X":
                cell.classList.add("boxOnStart");
                cell.dataset.cellType = "floor";
                const box2 = document.createElement("div");
                box2.classList.add("box2");
                box2.dataset.cellType = "box";
                cell.appendChild(box2);
                break;
            }
    }

          main.appendChild(mazeBoard);
        //previous maze 
}
function checkWin() {
  let winCount = 0;
  const blackBox = document.querySelectorAll(".boxOnStart");
  blackBox.forEach(element => {
      let foo = element.childElementCount;
      if (foo === 1 && element.firstChild.id !== "player") {
          winCount++;
      }
      if (winCount === 7) {

          setTimeout(function () {
              alert("You Win!");
          }, 500);
      }
  })
}
checkWin()
//Elizabeth Coach player set previous project
  const player = document.getElementById("player");
  let start = document.getElementById("start");
  start.appendChild(player);
      
  let currentPosition = start;

function findNextPosition(element, rowOffset, columnOffset) {
  const nextRowPosition = Number(element.dataset.rowIndex) + rowOffset;
  const nextColumnPosition = Number(element.dataset.cellIndex) + columnOffset;
  const nextCellElement = document.querySelector("[data-row-index = '" +nextRowPosition +"'][data-cell-index = '" +nextColumnPosition +"']");

  return nextCellElement;
}

//Bryan Coach Previous Maze project
document.addEventListener("keydown", event => {
  let nextCell;
  let followingCell;

  switch (event.key) {
    case "ArrowUp":
      nextCell = findNextPosition(currentPosition, -1, 0);
      followingCell = findNextPosition(nextCell, -1, 0);
      break;

    case "ArrowDown":
      nextCell = findNextPosition(currentPosition, +1, 0);
      followingCell = findNextPosition(nextCell, +1, 0);
      break;

    case "ArrowLeft":
      nextCell = findNextPosition(currentPosition, 0, -1);
      followingCell = findNextPosition(nextCell, 0, -1);
      break;

    case "ArrowRight":
      nextCell = findNextPosition(currentPosition, 0, +1);
      followingCell = findNextPosition(nextCell, 0, +1);
      break;
  }
  if (nextCell) {
    const box = nextCell.firstElementChild;

    if (box && box.dataset.cellType === "box" && followingCell.dataset.cellType === "floor" && followingCell.childElementCount === 0) {
      followingCell.appendChild(box);
    }

    if (nextCell.dataset.cellType === "floor" && nextCell.childElementCount === 0) {
      nextCell.appendChild(player);
      currentPosition = nextCell;
    }
  }
checkWin()
})